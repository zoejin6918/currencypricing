import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
/**
 * 
 * @author Zoe
 *
 */
public class CurrencyPricing
{
    private HashMap<String, Double> fxRates;
    
    public CurrencyPricing(String csvfile)   {
        fxRates = loadCsv(csvfile);
    }
    
    private HashMap<String, Double> loadCsv(String csvfile) {
        HashMap<String, Double> fxRate = new HashMap<String, Double>();
        BufferedReader reader;
        try {
            
            reader = new BufferedReader (new FileReader("src/main/resources/" + csvfile));
            reader.readLine(); // Skip first line
            String line = reader.readLine();
            while (line != null) {
                String currency = line.substring (0, 7);
                String rate = line.substring (8, line.length ());
                
                if (!rate.isEmpty () ) { // Rate format is valid.
                    double r = Double.parseDouble (rate);
                    fxRate.put(currency, r);
                }
                line = reader.readLine ();
            }
        } catch (IOException e) {
            e.printStackTrace ();
        }
        
        return fxRate;
    }
    
  
    public double getFXQuote(String currency1, String currency2) {
        double rate;
        String FX = currency1 + "," + currency2;
        if (fxRates.containsKey (FX)) {
            rate = fxRates.get (FX);
        } else {
            double rate1 = convertToUSD(currency1);
            double rate2 = convertToUSD(currency2);
            rate = rate1 / rate2;
        }
        return rate;
    }
    
    private double convertToUSD(String currency) {
        double rate = 0;
        String currencyToUSD = currency + ",USD"; 
        String USDToCurrency = "USD," + currency;
        if (fxRates.containsKey (currencyToUSD)) {
            rate = fxRates.get (currencyToUSD);
        } else if (fxRates.containsKey (USDToCurrency)) {
            rate = 1 / fxRates.get (currency);
        }
        if (rate == 0) {
            throw new IllegalArgumentException("Out of options!");
        }
        return rate;
    }
    
    public HashMap<String, Double> getFXRatePool() {
        return this.fxRates;
    }
    
    public void main(String[] args) {
        fxRates = loadCsv("rate.csv");
        double rate = getFXQuote ("AUD", "USD");
        System.out.println ("The FX rate of AUD/USD=" + rate);
        
    }

}
