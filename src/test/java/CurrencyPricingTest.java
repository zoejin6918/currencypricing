import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Zoe
 *
 */
public class CurrencyPricingTest
{
    CurrencyPricing tc;
    @Test
    public void testFXQuoteRegular ()
    {
        double rate1 = tc.getFXQuote ("AUD", "USD");
        assertEquals (1.0167, rate1, 0);
        
        double rate2 = tc.getFXQuote ("CNY", "CAD");
        assertEquals (0.1504, rate2, 0);
        
        double rate3 = tc.getFXQuote ("DKK", "GBP");
        assertEquals (0.1156, rate3, 0);
    }
    
    @Test
    public void testFXQuoteIndirect ()
    {
        double rate1 = tc.getFXQuote ("DKK", "CAD");
        assertFalse (tc.getFXRatePool().containsKey ("DKK,CAD"));
        assertTrue (tc.getFXRatePool().containsKey ("DKK,USD"));
        assertTrue (tc.getFXRatePool().containsKey ("CAD,USD"));
        assertEquals (0.1778, rate1, 0.0001);

    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void testFXQuoteNoOption() {
        tc.getFXQuote ("KKK", "USD");
    }
    
    @Before
    public void initial() {
        tc = new CurrencyPricing ("rates.csv");
    }

}
